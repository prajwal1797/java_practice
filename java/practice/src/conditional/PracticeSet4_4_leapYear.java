package conditional;

import java.util.Scanner;

public class PracticeSet4_4_leapYear {
	/*
	 * write the program to find whether the user entered year is leap year or not
	 */

	public static void main(String[] args) {
		System.out.println("enter the year");
		Scanner sc = new Scanner(System.in);

		int year = sc.nextInt();
		if ((year % 4) == 0) {
			System.out.println(year + " is leap year");

		} else {
			System.out.println(year + " is not leap year");
		}
	}

}
