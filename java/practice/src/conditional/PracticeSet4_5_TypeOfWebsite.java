package conditional;

import java.util.Scanner;

/*
 * program to find out the type of website from url
 * .com -> commercial website
 * .org -> organisation website
 * .in -> indian website
 */

public class PracticeSet4_5_TypeOfWebsite {
	public static void main(String[] args) {
		System.out.println("Enter your website");
		Scanner sc= new Scanner(System.in);
		String web= sc.next();
	if (web.endsWith(".org")) {
		System.out.println("your website is organisation");
	}
	else if (web.endsWith(".com")) {
		System.out.println("your website is commercial");
	}
	else if (web.endsWith(".in")) {
		System.out.println("your website is indian");
	}
		
	}

}
