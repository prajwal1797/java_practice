package conditional;

import java.util.Scanner;

public class PracticeSet4_2 {
	/*
	 * Calculate income tax paid by an employee to the government as per the slabs
	 * mentioned below;
	 * 
	 * 2.5-5 5% 5-10 20% above 10 30%
	 */

	public static void main(String[] args) {
		double tax = 0;

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter your income in lakhs");
		float income = sc.nextFloat();
	
		if (income < 2.5 || income == 2.5) {
			tax = 0;
			System.out.println("tax to be paid " + (tax * income));

		} else if (income > 2.5 && income < 5) {
			tax = 0.05;
			System.out.println("tax to be paid " + (tax * income) + "lakhs");

		}

		else if (income > 5 && income < 10) {
			tax = 0.2;
			System.out.println("tax to be paid " + (tax * income) + "  lakhs");

		} else if (income > 10) {
			tax = 0.3;
			System.out.println("tax to be paid " + (tax * income) + "  lakhs");

		}
	}

}
