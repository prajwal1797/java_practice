package arrays;

public class Arrays {
	/*
	 * collections of similar data types and its useful for faster data retrieval
	 * 
	 * 3 types to create array 1.declaration and memory allocation...int[] marks=
	 * new int[4];
	 * 
	 * 2.declaration and then memory allocation int[] marks;.....marks = new int[4];
	 * 
	 */
	public static void main(String[] args) {

//		int[] marks= new int[3];
//		marks[0]=99;
//		marks[1]=89;
//		marks[2]=100;
//		
//		System.out.println(marks[2]);

		//3. declaration memory allocation and initialization together
		
		int[] marks = { 99, 89, 100 };
		System.out.println(marks[2]);

	}

}
