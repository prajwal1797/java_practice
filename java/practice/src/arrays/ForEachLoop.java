package arrays;

public class ForEachLoop {

	public static void main(String[] args) {

		int[] marks = { 99, 89, 100 };
	//	System.out.println(marks.length);   //to find the length of the array
		
		// to display array
		
	for(int i=0; i<marks.length; i++) {
			System.out.println(marks[i]);
		}
		
		System.out.println("**in reverse order**");
		
		for(int i=marks.length-1; i>=0; i--) {
			System.out.println(marks[i]);
		}

	}
}
