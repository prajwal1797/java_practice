package practiceSet;

public class PracticeSet1 {

	public static void main(String[] args) {

		// convert string to lowercase

		String name = "PRAJWAL";
		name = name.toLowerCase();
		System.out.println(name);

		// REPLACE SPACE WITH UNDERSCORE

		String text = "PRAJWAL MAHI";
		text = text.replace(" ", "_").toLowerCase();
		System.out.println(text);

		// to detect double and triple spaces
		/*
		 * gives -1 op if does not contain. here it triple space is not present
		 */
		String myString = "This string contains  double and triple space";
		System.out.println(myString.indexOf("  "));
		System.out.println(myString.indexOf("   "));

		// program to format the following letter using escape sequence cht

		String letter = "This program is,\n\tfor java \n\tpractice";
		System.out.println(letter);

	}

}
