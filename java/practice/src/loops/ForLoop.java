package loops;

import java.util.Iterator;

public class ForLoop {

	/*
	 * used to execute a piece of code for specific number of time
	 */
	public static void main(String[] args) {
		
		for(int i=0; i<=10;i++) {
			System.out.println(i);
		}

	}

}
