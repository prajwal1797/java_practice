package loops;

import java.util.Scanner;

public class ReverseString {

	public static void main(String[] args) {
		System.out.println("Enter your word");
		Scanner sc = new Scanner(System.in);
		String originalStr = sc.next();
		String reversedStr = "";

		for (int i = 0; i < originalStr.length(); i++) {
			reversedStr = originalStr.charAt(i) + reversedStr;
			System.out.println(reversedStr);
		}

		System.out.println("Reversed string: " + reversedStr);
	}

}