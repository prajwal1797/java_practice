package loops;

import java.util.Iterator;
import java.util.Scanner;

public class OddNumbers {
	/*
	 * 2n for even....2n+1 for odd
	 */

	public static void main(String[] args) {
		System.out.println("Enter number of odd number required");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		for (int i = 1; i < n; i++) {
			System.out.println(2 * i + 1);

		}
	}

}
