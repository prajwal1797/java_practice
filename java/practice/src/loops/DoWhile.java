package loops;

public class DoWhile {
	/*
	 * similar to while but it execute the code atleast once whithout checking condition
	 * 
	 * 
	 */

	public static void main(String[] args) {
		int i =0;
		
		do {
			System.out.println("do block");
			System.out.println(i);
			i++;
			
		} while (i<5);

	}

}
