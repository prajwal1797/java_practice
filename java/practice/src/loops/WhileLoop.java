package loops;

public class WhileLoop {
	/*
	 * Loop Sometimes we want our program to execute a few set of instruction over
	 * and over
	 * 
	 * 
	 * while loop:- if the condition is true the jvm enter into the loop and execute
	 * the code until it gets false
	 * 
	 */

	public static void main(String[] args) {

		int i = 100;

		while (i <= 200) {
			System.out.println(i);
			i++;

		}

	}

}
